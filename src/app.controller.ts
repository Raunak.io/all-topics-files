import {
  Controller,
  Get,
  Post,
  Put,
  Patch,
  Param,
  Delete,
  Req,
  HttpCode,
  Redirect,
  Query,
  Body,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { AppService } from './app.service';
import { Request } from 'express';
import { Observable, of } from 'rxjs';

class CreateDto {  // the data transfer object
  name: string;
  age: number;
}

// @Controller()
@Controller('api')
export class AppController {
  constructor(private readonly appService: AppService) {}

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  // @Get()
  // test(): string {
  //   return 'FOR TESTING PURPOSE ONLY';
  // }
  @Get('getting')
  test(@Req() request: Request): string {
    return 'FOR TESTING PURPOSE ONLY';
  }
  // @Get()
  // test(@Req() request: Request): string {
  //   return 'FOR TESTING PURPOSE ONLY';
  // }
  @Get(':id')
  test1(@Param() params): string {
    return 'FOR TESTING PURPOSE ONLY';
  }

  @Get()
  async fi(): Promise<any[]> { // return type is promise of any []
    return [];
  }

  @Get()
  fill(): Observable<any[]> {// return observable 
    return of([]);
  }

  @Post()
  @HttpCode(204)
  test2(@Body() createDto: CreateDto): string { // used dto i.e data transfer object
   console.log(createDto);
    return 'FOR TESTING PURPOSE ONLY';
  }

  @Put()
  test3(@Body() createDto: CreateDto): string {
    console.log(createDto);
    return 'FOR TESTING PURPOSE ONLY';
  }

  @Patch()
  test4(@Body() createDto: CreateDto): string {
    console.log(createDto);
    return 'FOR TESTING PURPOSE ONLY';
  }
  @Delete(':id')
  test5(@Param('id') id: string): string {
    console.log(id);
    return 'FOR TESTING PURPOSE ONLY';
  }

  @Get('ab*cd')
  findAll(): string {
    return 'This route uses a wildcard';  // this is for wild card patterns 
  }
  @Get('test')
  @Redirect('https://test.test.com', 302)   // if you wanna redirect to any other url
  getDocs(@Query('version') version): any {
    if (version && version === '5') {
      return { url: 'https://test.test.com/v5/' };
    }
  }
}
